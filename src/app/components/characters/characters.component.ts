import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../../services/characters.service'

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css'],
  providers: [CharacterService]
})
export class CharactersComponent implements OnInit {
  public characters: any;
  public comic: any;
  public onlyComic: any;
  public comicR: any;
  public onlyComicR: any;
  public favourites: any[] = [];
  public characterFilter: any = { name: '' };
  p: number = 1;
  
  constructor(
    private _characterService: CharacterService
  ) { 
    
  }

  ngOnInit(): void {
    this._characterService.getCharacters().subscribe(
      (response)=>{
        if(response){
          this.characters = response.data.results;
        }else{}
        console.log(this.characters);
      }, (error)=>{
        //console.log('Error: ' + error);
      }
    );

    Object.values(localStorage).forEach((e: any, i) => {
      this.favourites.push(JSON.parse(e));
    });
  }


  getComic(urlComic:string){
    this._characterService.getComicInd(urlComic).subscribe(
      (responseComic)=>{
        if(responseComic){
          this.comic = responseComic;
          this.onlyComic = this.comic.data.results[0];
        }else{}
        //console.log(this.onlyComic);
      }, (error)=>{
        //console.log(error);
      }
    );
  }


  closeModal(){
    this.onlyComic = '';
  }

  guardarFavorito(id:number, content:any){
    localStorage.setItem( id + '_id', JSON.stringify(content));
    this.favourites = [];
    Object.values(localStorage).forEach((e: any, i) => {
      this.favourites.push(JSON.parse(e));
    });
    console.log(this.favourites);
  }

  deleteFavorite(id: number){
    localStorage.removeItem(id + '_id');
    this.favourites = [];
    Object.values(localStorage).forEach((e: any, i) => {
      this.favourites.push(JSON.parse(e));
    });
  }

  getRadomComics(){
    this._characterService.getRandomComic().subscribe(
      (responseR)=>{
        if(responseR){
          this.comicR = responseR;
          this.onlyComicR = this.comicR.data.results;

          for(let i = 0; i <= 2; i++){
            var randomElement = this.onlyComicR[Math.floor(Math.random()*this.onlyComicR.length)];
            this.guardarFavorito(randomElement.id, randomElement)
            //console.log(this.onlyComicR);
          }

        }else{}
      }, (error)=>{
      }
    );
  }

}
